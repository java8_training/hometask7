public interface EmpConst {
    Employee get(String name, String account, double salary);
}
