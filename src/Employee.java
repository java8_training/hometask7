public class Employee {
    String empName, accountName;
    double empSalary;

    Employee(String name, String account, double salary){
        empName = name;
        accountName = account;
        empSalary = salary;
    }

    @Override
    public String toString(){
        return "Emp Name : "+empName+"\tEmp Account : "+accountName+"\tEmp Salary : "+empSalary;
    }
}
