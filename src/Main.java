public class Main {
    public static void main(String[] args) {
        EmpConst empConstructor=Employee::new;
        Employee emp1 = empConstructor.get("Raj","Testing",10000);
        Employee emp2 = empConstructor.get("Steve","Development",12000);
        Employee emp3 = empConstructor.get("Dave","HR",15000);
        System.out.println(emp1.toString());
        System.out.println(emp2.toString());
        System.out.println(emp3.toString());
    }
}
